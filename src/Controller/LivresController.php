<?php

namespace App\Controller;

use App\Entity\Exemplaire;
use App\Entity\Livre;
use App\Repository\LivreRepository;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LivresController extends AbstractController
{

    public function listeLivres()
    {

        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();

        //Passage du tableau au template
        return $this->render('livres.html.twig',['titre'=>"Liste des livres",'livres'=>$livres]);
    }


    public function afficherLivre($id)
    {
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);

        $exemplaires = $this->getDoctrine()->getRepository(Exemplaire::class)->findByLivre($livre);

        return $this->render('livre.html.twig',['livre'=>$livre,'lesExemplaires'=>$exemplaires]);
    }
}