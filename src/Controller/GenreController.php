<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GenreController extends AbstractController
{
    /**
     * @Route("/genre", name="lesGenres")
     */
    public function index()
    {
        $genres = $this->getDoctrine()->getRepository(Genre::class)->findAll();

        return $this->render('genre/index.html.twig', [
            'lesGenres' => $genres,
        ]);
    }

    /**
     * @Route("/genre/{id}", name="leGenre")
     */
    public function detailsGenre($id)
    {
        $genre = $this->getDoctrine()->getRepository(Genre::class)->find($id);

        $livres = $this->getDoctrine()->getRepository(Livre::class)->findByGenre($genre);

        return $this->render('genre/legenre.html.twig', [
            'leGenre' => $genre,
            'lesLivres' => $livres
        ]);
    }
}
