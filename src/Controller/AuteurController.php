<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AuteurController extends AbstractController
{
    /**
     * @Route("/auteur", name="auteur")
     */
    public function index()
    {
        $auteurs = $this->getDoctrine()->getRepository(Auteur::class)->findAll();
        return $this->render('auteur/index.html.twig', [
            'controller_name' => 'AuteurController',
            'auteurs' => $auteurs
        ]);
    }

    /**
     * @Route("/auteur/{id}/livre", name="auteurLivres")
     */
    public function auteurLivres($id)
    {
        $auteur = $this->getDoctrine()->getRepository(Auteur::class)->find($id);

        $livres = $this->getDoctrine()->getRepository(Livre::class)->findByAuteur($auteur);

        return $this->render('auteur/livres.html.twig', [
            'auteur'=>$auteur,
            //'livres' => $auteur->getLesLivres()
            'livres' => $livres
        ]);
    }
}
