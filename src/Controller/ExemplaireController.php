<?php

namespace App\Controller;

use App\Entity\Exemplaire;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ExemplaireController extends AbstractController
{
    /**
     * @Route("/exemplaire", name="exemplaire")
     */
    public function index()
    {
        $exemplaires = $this->getDoctrine()->getRepository(Exemplaire::class)->findAll();

        return $this->render('exemplaire/index.html.twig', [
            'lesExemplaires' => $exemplaires,
        ]);
    }

    /**
     * @Route("/emprunts", name="emprunts")
     */
    public function listeEmprunts()
    {
        $this->denyAccessUnlessGranted('ROLE_LECTEUR', null, 'User tried to access a page without having ROLE_LECTEUR');
        $lecteur = $this->getUser();
        $exemplaires = $this->getDoctrine()->getRepository(Exemplaire::class)->findByEmprunteur($lecteur);

        return $this->render('exemplaire/index.html.twig', [
            'lesExemplaires' => $exemplaires,
        ]);
    }
}
