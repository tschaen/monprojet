<?php

namespace App\Controller;

use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('index.html.twig',['titre'=>"Page d'accueil"]);
    }

    public function aproposAction()
    {
        //$this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        $this->denyAccessUnlessGranted('ROLE_LECTEUR', null, 'User tried to access a page without having ROLE_LECTEUR');


        $livres = array("Livre1","Livre2","Livre3");
        return $this->render('apropos.html.twig',['titre'=>"A propos de mon projet"]);
        /*return new Response(
            '<html><body>A propos de MonProjet</body></html>');*/
    }

    public function connexion()
    {
        return new Response(
            '<html><body> Page de connexion en cours de construction ...</body></html>'
        );
    }

}