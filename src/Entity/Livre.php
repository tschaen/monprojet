<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Auteur;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivreRepository")
 * @ORM\Table(name="livre")
 */
class Livre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="livre_idlivre_seq")
     * @ORM\Column(type="integer",name="idlivre")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $titre;

    /**
     * @ORM\Column(type="string")
     */
    private $resume;

    /**
     * @ORM\Column(type="string")
     */
    private $isbn;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Auteur")
     * @ORM\JoinColumn(name="idauteur", referencedColumnName="idauteur")
     */

    private $auteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Genre")
     * @ORM\JoinTable(name="appartenir",
     *  joinColumns={@ORM\JoinColumn(name="idlivre",referencedColumnName="idlivre")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="idgenre",referencedColumnName="idgenre")})
     */
    private $genres;

    private function __construct()
    {
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        if ($this->genres->contains($genre)) {
            $this->genres->removeElement($genre);
        }

        return $this;
    }

}