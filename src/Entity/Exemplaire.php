<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="App\Repository\ExemplaireRepository")
* @ORM\Table(name="exemplaire")
*/

class Exemplaire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="numero")
     */
    private $numero;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Livre")
     * @ORM\JoinColumn(name="idlivre", referencedColumnName="idlivre")
     */
    private $livre;

    /**
     * @ORM\Column(type="date")
     */
    private $dateretour;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lecteur")
     * @ORM\JoinColumn(name="emprunteur", referencedColumnName="idelecteur")
     */
    private $emprunteur;

    /**
     * @return mixed
     */
    public function getEmprunteur()
    {
        return $this->emprunteur;
    }

    /**
     * @param mixed $emprunteur
     */
    public function setEmprunteur($emprunteur): void
    {
        $this->emprunteur = $emprunteur;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function getDateretour(): ?\DateTimeInterface
    {
        return $this->dateretour;
    }

    public function setDateretour(\DateTimeInterface $dateretour): self
    {
        $this->dateretour = $dateretour;

        return $this;
    }

    public function getLivre(): ?Livre
    {
        return $this->livre;
    }

    public function setLivre(?Livre $livre): self
    {
        $this->livre = $livre;

        return $this;
    }
}