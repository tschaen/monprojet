<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity(repositoryClass="App\Repository\GenreRepository")

 * @ORM\Table(name="genre")

 */

class Genre

{

    /**

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="SEQUENCE")

     * @ORM\SequenceGenerator(sequenceName="genre_idgenre_seq")

     * @ORM\Column(type="integer",name="idgenre")

     */

    private $id;


    /**

     * @ORM\Column(type="string")

     */

    private $libelle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}