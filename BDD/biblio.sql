CREATE TABLE genre (
	idgenre serial NOT NULL,
	libelle varchar NULL,
	CONSTRAINT genre_pk PRIMARY KEY (idgenre)
);

CREATE TABLE auteur (
	idauteur serial NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	datedenaissance date NULL,
	datededeces date NULL,
	CONSTRAINT auteur_pk PRIMARY KEY (idauteur)
);

CREATE TABLE livre (
	idlivre serial NOT NULL,
	titre varchar NULL,
	resume varchar NULL,
	isbn varchar NULL,
	idauteur int4 NULL,
	CONSTRAINT livre_pk PRIMARY KEY (idlivre),
	CONSTRAINT livre_fk FOREIGN KEY (idauteur) REFERENCES auteur(idauteur)
);

CREATE TABLE appartenir (
	idlivre int4 NOT NULL,
	idgenre int4 NOT NULL,
	CONSTRAINT appartenir_pk PRIMARY KEY (idgenre,idlivre),
	CONSTRAINT appartenir_fk FOREIGN KEY (idgenre) REFERENCES genre(idgenre),
	CONSTRAINT appartenir_fk_1 FOREIGN KEY (idlivre) REFERENCES livre(idlivre)
);

CREATE TABLE lecteur (
	idelecteur serial NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	datedenaissance date NULL,
	datepremiereadhesion date NULL,
	datefinadhesion date NULL,
	CONSTRAINT lecteur_pk PRIMARY KEY (idelecteur)
);

CREATE TABLE exemplaire (
	numero int4 NOT NULL,
	dateretour date NULL,
	idlivre int4 NOT NULL,
	emprunteur int4 NULL,
	CONSTRAINT exemplaire_pk PRIMARY KEY (numero,idlivre),
	CONSTRAINT exemplaire_fk FOREIGN KEY (idlivre) REFERENCES livre(idlivre),
	CONSTRAINT exemplaire_fk_1 FOREIGN KEY (emprunteur) REFERENCES lecteur(idelecteur)
);

INSERT INTO auteur (nom,prenom,datedenaissance,datededeces) VALUES 
('Heliot','Johan','1970-08-23',NULL)
,('Damasio','Alain','1969-08-01',NULL)
,('Barjavel','René','1911-01-24','1985-11-24')
,('Lynch','Scott','1978-04-02',NULL)
;

INSERT INTO genre (libelle) VALUES 
('Science-fiction')
,('Fantasy')
;

INSERT INTO livre (titre,resume,isbn,idauteur) VALUES 
('Ravage','Ravage présente le naufrage d''une société mature, dans laquelle, un jour, l''électricité disparaît et plus aucune machine ne peut fonctionner. Les habitants, anéantis par la soudaineté de la catastrophe, sombrent dans le chaos, privés d''eau courante, de lumière et de moyens de déplacement.','978-2070362387',3)
,('La Nuit des temps','Dans le grand silence blanc de l''Antarctique, les membres d''une mission des Expéditions polaires françaises s''activent à prélever des carottes de glace. L''épaisseur de la banquise atteint plus de 1 000 mètres, les couches les plus profondes remontant à 900 000 ans...
C''est alors que l''incroyable intervient : les appareils sondeurs enregistrent un signal provenant du niveau du sol. Il y a un émetteur sous la glace. La nouvelle éclate comme une bombe et les journaux du monde entier rivalisent de gros titres : " Une ville sous la glace ", " Un cœur sous la banquise ", etc. Que vont découvrir les savants et les techniciens qui, venus du monde entier, forent la glace à la rencontre du mystère ?
Reportage, épopée et chant d''amour passionné, La Nuit des temps est tout cela à la fois.','978-2258152830',3)
,('La zone du dehors','2084. Orwell est loin désormais. Le totalitarisme a pris les traits bonhommes de la social-démocratie. Souriez, vous êtes gérés ! Le citoyen ne s''opprime plus : il se fabrique. À la pâte à norme, au confort, au consensus. Copie qu''on forme, tout simplement. Au cœur de cette glu, un mouvement, une force de frappe, des fous : la Volte. Le Dehors est leur espace, subvertir leur seule arme. Emmenés par Capt, philosophe et stratège, le peintre Kamio et le fulgurant Slift que rien ne bloque ni ne borne, ils iront au bout de leur volution. En perdant beaucoup. En gagnant tout. Premier roman, ici réécrit, La Zone du Dehors est un livre de combat contre nos sociétés de contrôle. Celle que nos gouvernements, nos multinationales, nos technologies et nos médias nous tissent aux fibres, tranquillement. Avec notre plus complice consentement. Peut-être est-il temps d''apprendre à boxer chaos debout contre le swing de la norme ?','978-2070464241',2)
,('La horde du contrevent','Un groupe d''élite, formé dès l''enfance à faire face, part des confins d''une terre féroce, saignée de rafales, pour aller chercher l''origine du vent. Ils sont vingt-trois, un bloc, un nœud de courage : la Horde. Ils sont pilier, ailier, traceur, aéromaître et géomaître, feuleuse et sourcière, troubadour et scribe. Ils traversent leur monde debout, à pied, en quête d''un Extrême-Amont qui fuit devant eux comme un horizon fou. Expérience de lecture unique, La Horde du Contrevent est un livre-univers qui fond d''un même feu l''aventure et la poésie des parcours, le combat nu et la quête d''un sens profond du vivant qui unirait le mouvement et le lien. Chaque mot résonne, claque, fuse : Alain Damasio joue de sa plume comme d''un pinceau, d''une caméra ou d''une arme… Chef-d''œuvre porté par un bouche-à-oreille rare, le roman a été logiquement récompensé par le Grand Prix de l''Imaginaire.','978-2070464234',2)
,('Les furtifs','Ils sont là parmi nous, jamais où tu regardes, à circuler dans les angles morts de la vision humaine. On les appelle les furtifs. Des fantômes ? Plutôt l''exact inverse : des êtres de chair et de sons, à la vitalité hors norme, qui métabolisent dans leur trajet aussi bien pierre, déchet, animal ou plante pour alimenter leurs métamorphoses incessantes. Lorca Varèse, sociologue pour communes autogérées, et sa femme Sahar, proferrante dans la rue pour les enfants que l''éducation nationale, en faillite, a abandonnés, ont vu leur couple brisé par la disparition de leur fille unique de quatre ans, Tishka - volatisée un matin, inexplicablement. Sahar ne parvient pas à faire son deuil alors que Lorca, convaincu que sa fille est partie avec les furtifs, intègre une unité clandestine de l''armée chargée de chasser ces animaux extraordinaires. Là, il va découvrir que ceux-ci naissent d''une mélodie fondamentale, le frisson, et ne peuvent être vus sans être aussitôt pétrifiés. Peu à peu il apprendra à apprivoiser leur puissance de vie et, ainsi, à la faire sienne. Les Furtifs vous plonge dans un futur proche et fluide où le technococon a affiné ses prises sur nos existences. Une bague interface nos rapports au monde en offrant à chaque individu son alter ego numérique, sous forme d''IA personnalisée, où viennent se concentrer nos besoins vampirisés d''écoute et d''échanges. Partout où cela s''avérait rentable, les villes ont été rachetées par des multinationales pour être gérées en zones standard, premium et privilège selon le forfait citoyen dont vous vous acquittez. La bague au doigt, vous êtes tout à fait libres et parfaitement tracés, soumis au régime d''auto-aliénation consentant propre au raffinement du capitalisme cognitif.','978-2370490742',2)
,('Les Mensonges de Locke Lamora','On l''appelle la Ronce de Camorr. Un bretteur invincible, un maître voleur. La moitié de la ville le prend pour le héros des miséreux. L''autre moitié pense qu''il n''est qu''un mythe. Les deux moitiés n''ont pas tort. En effet, de corpulence modeste et sachant à peine manier l''épée, locke lamora est, à son grand dam, la fameuse Ronce. Les rumeurs sur ses exploits sont en fait des escroqueries de la pire espèce, et lorsque locke vole aux riches, les pauvres n''en voient pas le moindre sou. Il garde tous ses gains pour lui et sa bande : les Salauds Gentilshommes.','978-2352947196',4)
;

INSERT INTO appartenir (idlivre,idgenre) VALUES 
(1,1)
,(2,1)
,(3,1)
,(4,1)
,(5,1)
,(6,2)
;

INSERT INTO lecteur (nom,prenom,datedenaissance,datepremiereadhesion,datefinadhesion) VALUES 
('Lannister','Tyrion','1975-02-03','2015-09-01','2020-05-04')
,('Targaryen','Daenerys','1981-03-04','2017-05-25','2019-12-25')
,('Snow','Jon','1982-04-05','2018-03-02','2019-08-15')
,('Stark','Arya','1986-05-06','2019-02-02','2020-02-01')
;

INSERT INTO exemplaire (numero,dateretour,idlivre,emprunteur) VALUES 
(1,'2020-12-10',1,1)
,(2,'2020-10-15',1,2)
,(1,'2020-12-10',2,1)
,(1,'2020-12-10',3,1)
,(2,'2020-11-30',3,4)
,(1,'2020-12-10',4,1)
,(2,NULL,4,NULL)
,(3,'2020-11-30',4,4)
,(1,'2020-12-10',5,1)
,(1,'2020-12-10',6,1)
,(2,NULL,6,NULL)
,(3,NULL,6,NULL)
;
